import { Component, OnInit } from '@angular/core';
import {Producto} from './producto.interface';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  public productos: Producto[]; 
  public producto: Producto; 
  public showForm = false;

  constructor() {
    this.productosArtificales();
   }

  ngOnInit(): void {
  }

  newProducto(){
    this.showForm = true; 
    this.producto= {
      id:0,
      nombre:'',
      precio:0,
      stock:0
    }
  }

  productosArtificales(){
    this.productos = []; 
    let p1: Producto = {
      id:1,
      nombre:'Sandía',
      precio:2000,
      stock:5
    }
    let p2: Producto = {
      id:2,
      nombre:'Mango',
      precio:1000,
      stock:5
    }
    let p3: Producto = {
      id:3,
      nombre:'Melon',
      precio:800,
      stock:5
    }
    let p4: Producto = {
      id:4,
      nombre:'Uvas',
      precio:2800,
      stock:5
    }  
    let p5: Producto = {
      id:5,
      nombre:'Fresas',
      precio:800,
      stock:5
    }
    this.productos.push(p1); 
    this.productos.push(p2); 
    this.productos.push(p3); 
    this.productos.push(p4); 
    this.productos.push(p5); 
  }

  save(){
    console.log(this.producto.nombre); 
    this.productos.push(this.producto); 
    this.showForm = false; 
  }



}
