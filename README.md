# Aplicación SPA con angular.

## Guía de uso aplicación SPA

Paso 1: Instalar dependencias con:
    
    npm install

Paso 2: Correr servidor.
    
    ng serve

Paso 3: Para hacer uso de este debemos tomar la URL generada y dirigirnos a la busqueda del navegador.
por lo general es http://localhost:4200/

Luego se desplega una pantalla con el main principal, donde se verá un form

![](imagenes/contact.png "Contactanos")

Se puede dirigir al apartado de productos

![](imagenes/productos.png "Productos")

Y se desplegara una ventana donde se ven productos, que estan previamente creados

Luego pude agregar uno tocando el boton agregar

![](imagenes/producto_agregar.png "Agregar")

Se desplega un form, donde agrega producto

## Caracteristicas SPA

1.  Se carga en el Html principal los componentes que se desean dirigir.
2.  Los datos estan creados manualmente pero son independientes del frontend y backend, ya que pueden ser extraidos de cualquier backend.
3.  Se muestra la URL en el navegador permitiendo al usuario escribir en el buscador a la ruta que desea dirigirse. 
4. Y de acuerdo al punto anterior al tener url, tambien permiti al usuario regresar con las flechas del buscador, ya que el buscador guarda las url por donde el usuario va navegando

## Aspectos generales.

Esta aplicación se puede descargar, cuenta con un formulario de contacto vacio, un registrar productos, los cuales pueden ser conectados para guardar datos utilizando tecnologias como laravel, .net core, entre otros.

